FROM ubuntu:16.04
MAINTAINER Fabian Off <fabian+docker-gcc-arm@simpletechs.net>
LABEL Description="Image for building arm-embedded projects"

# Install any needed packages specified in requirements.txt
RUN apt update && \
    apt upgrade -y && \
    apt install -y \
# Development files
      build-essential \
      git \
      bzip2 \
      curl \
      bison \
      flex \
      python \
      swig \
      python-dev autoconf pkg-config libusb-1.0-0-dev libcrypto++ \
      bc device-tree-compiler cbootimage \
      && \
    apt clean

# ENV ARM_VERSION 7-2017q4/gcc-arm-none-eabi-7-2017-q4-major # - this is to be supplied via --build-arg ARM_VERSION=7-2017q4/gcc-arm-none-eabi-7-2017-q4-major
# Install toolchain in extra run to re-use base container
RUN curl -L https://developer.arm.com/-/media/Files/downloads/gnu-rm/$ARM_VERSION-linux.tar.bz2 | tar -xjv --strip-components=1 -C /usr/local
